﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeNews.Model
{
    public class NewsItem
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string HeadLine { get; set; }
        public string Subhead { get; set; }
        public string Dateline { get; set; }
        public string Image { get; set; }
    }
    public class NewsManager
    {
        public static void GetNews(string category, ObservableCollection<NewsItem> newsItems)
        {
            var allItems = GetNewsItems();
            var filteredNewsItem = allItems.Where(p => p.Category == category).ToList();
            newsItems.Clear();
            filteredNewsItem.ForEach(p => newsItems.Add(p));
        }
        private static List<NewsItem> GetNewsItems()
        {
            var items = new List<NewsItem>();

            items.Add(new NewsItem()
            {
                Id = 1,
                Category = "Financial",
                HeadLine = "Lorem Ipsum ",
                Subhead = "doro sit amet",
                Dateline = "Nunc tristique nec",
                Image = "Assets/Images02/Finankial1.png"
            });
            items.Add(new NewsItem()
            {
                Id = 2,
                Category = "Financial",
                HeadLine = "Etiam ac felis viverra",
                Subhead = "vulputate nisl ac,aliquet nisi",
                Dateline = "tortor porttitor,eu fermentum ante congue",
                Image = "Assets/Images02/Finncial2.png"
            });
            items.Add(new NewsItem()
            {
                Id = 3,
                Category = "Financial",
                HeadLine = "Sed quis hendrerit lorem,quis interdum dolor",
                Dateline = "in viverra metus facilisis sed",
                Image = "Assets/Images02/Finncial3.png"
            });
            items.Add(new NewsItem()
            {
                Id = 4,
                Category = "Financial",
                HeadLine = "Proin sem neque",
                Subhead = "aliquet quis ipsum tincidunt",
                Dateline = "integer eleifend",
                Image = "Assets/Images02/Finncial4.png"
            });
            items.Add(new NewsItem()
            {
                Id = 5,
                Category = "Financial",
                HeadLine = "Mauris bibendum non leo vitae tempor",
                Subhead = "In nisi tortor,eleifend sed ipsum eget",
                Dateline = "Curabitur dictum augue vitae elementum ultrices",
                Image = "Assets/Images02/Finncial5.png"
            });
            items.Add(new NewsItem()
            {
                Id = 6,
                Category = "Food",
                HeadLine = "Lorem ipsum",
                Subhead = "dolor sit amet",
                Dateline = "Nunc tristique nec",
                Image = "Assets/Images02/Food1.png"
            });
            items.Add(new NewsItem()
            {
                Id = 7,
                Category = "Food",
                HeadLine = "Etiam ac felis viverra",
                Subhead = "vulputate nisl ac,aliquet nisi",
                Dateline = "tortor porttitor,eu fermentum ante congue",
                Image = "Assets/Images02/Food2.png"
            });
            items.Add(new NewsItem()
            {
                Id = 8,
                Category = "Food",
                HeadLine = "Integer sed turpis erat",
                Subhead = "Send quil hendrerit lorem,quis interdum dolor",
                Dateline = "in viverra metus facilisis sed",
                Image = "Assets/Images02/Food3.png"
            });
            items.Add(new NewsItem()
            {
                Id = 9,
                Category = "Food",
                HeadLine = "Proin sem neque",
                Subhead = "aliquet quis ipsum tincidunt",
                Dateline = "Intege eleifend",
                Image = "Assets/Images02/Food4.png"
            });
            items.Add(new NewsItem()
            {
                Id = 10,
                Category = "Food",
                HeadLine = "Mauris bibedum non leo vitae tempor",
                Subhead = "In nisl tortor,eleifend sed ipsum eget",
                Dateline = "Curabbitur dictum augue vitae elementum ultrices",
                Image = "Assets/Images02/Food5.png"
            });
            return items;
        }
    }
}

